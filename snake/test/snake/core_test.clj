(ns snake.core-test
  (:require [clojure.test :refer :all]
            [snake.core :refer :all]))

(deftest test-add-points
  (testing "add-points"
    (is (add-points [10 10] [-1 0]) [9 10])))

(deftest test-point-to-screen-rect
  (testing "point-to-screen-rect"
    (is (point-to-screen-rect [5 10]) (list 50 100 10 10))))

(deftest test-lose?
  (testing "should be false"
    (is (lose? {:body [[1 1] [1 2] [1 3]]}) false)))

(deftest test-lose?
  (testing "should be true"
    (is (lose? {:body [[1 1] [1 2] [1 1]]}) true)))

(deftest test-eats?
  (testing "should be false"
    (is (eats? {:body [[1 1] [1 2]]} {:location [2 2]}) false)))

(deftest test-eats?
  (testing "should be true"
    (is (eats? {:body [[2 2] [1 2]]} {:location [2 2]}) true)))
