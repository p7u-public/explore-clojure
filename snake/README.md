# Snake

The Snake game features a player-controlled snake that moves around a game
grid hunting for an apple. When the snake eats an apple, it grows longer by
a segment, and a new apple appears. If the snake reaches a certain length,
the player wins. But if the snake crosses over its own body, the game is lost.

The implementation is based on ideas and code from the book "Programming 
Clojure" (2nd edition) by Stuart Halloway
